module Task
  use mpi
  implicit none
  contains

  subroutine GetMaxCoordinates(A, x1, y1, x2, y2)
    implicit none
    real(8), intent(in), dimension(:,:) :: A
    integer(4), intent(out) :: x1, y1, x2, y2
    integer(4) :: n, L, R, Up, Down, m, tmp, mpiSize, mpiRank, mpiErr, i, Coord(4), NumMaxVal
    integer(4), dimension(MPI_STATUS_SIZE) :: status
    real(8), allocatable :: current_column(:), maxvalue(:), max_sum(:)
    real(8) :: current_sum
    
    call mpi_comm_size(MPI_COMM_WORLD, mpiSize, mpiErr)
    call mpi_comm_rank(MPI_COMM_WORLD, mpiRank, mpiErr)
    allocate(maxvalue(0:mpiSize-1))
    allocate(max_sum(0:mpiSize-1))
    
    m = size(A, dim=1) 
    n = size(A, dim=2) 

    allocate(current_column(m))

    Coord = 1
    max_sum = 0
    max_sum(mpiRank) = A(1,1)
    

    do L = mpiRank+1, n, mpiSize
      current_column = A(:, L)

      do R = L, n
        if (R > L) then
          current_column = current_column + A(:, R)
        endif 
         
        call FindMaxInArray(current_column, current_sum, Up, Down)

        if (current_sum > max_sum(mpiRank) ) then
          max_sum(mpiRank) = current_sum
          Coord(1) = Up
          Coord(2) = Down
          Coord(3) = L
          Coord(4) = R
        endif
      end do
    end do

    call mpi_reduce(max_sum, maxvalue, mpiSize, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, mpiErr)
    NumMaxVal = maxloc(maxvalue, 1)-1
    
    call mpi_bcast(NumMaxVal, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, mpiErr)
    call mpi_bcast(Coord, 4, MPI_INTEGER4, NumMaxVal, MPI_COMM_WORLD, mpiErr)
    
    x1 = Coord(1)
    x2 = Coord(2)
    y1 = Coord(3)
    y2 = Coord(4)

    deallocate(current_column)
    deallocate(maxvalue)
    deallocate(max_sum)
  end subroutine

  subroutine FindMaxInArray(A, Summ, Up, Down)
    implicit none
    real(8), intent(in), dimension(:) :: A
    integer(4), intent(out) :: Up, Down
    real(8), intent(out) :: Summ
    real(8) :: cur_sum
    integer(4) :: minus_pos, i

    Summ = A(1)
    Up = 1
    Down = 1
    cur_sum = 0
    minus_pos = 0

    do i=1, size(A)
      cur_sum = cur_sum + A(i)
      if (cur_sum > Summ) then
        Summ = cur_sum
        Up = minus_pos + 1
        Down = i
      endif
     
      if (cur_sum < 0) then
        cur_sum = 0
        minus_pos = i
      endif
    enddo

  end subroutine
end module